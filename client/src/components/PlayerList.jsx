import React from 'react'

const PlayerList = ({players, selectEdit}) => {

return (
<div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Experience</th>
                <th scope="col">Level</th>
                <th scope="col" className='text-center'>Action</th>
            
            </tr>
        </thead>
        <tbody>
            {players.map((player, index) => (
                <tr key={index}>
                    <td>{player.username}</td>
                    <td>{player.email}</td>
                    <td>{player.exp}</td>
                    <td>{player.lvl}</td>
                    <td className='text-center'> <button onClick={() => selectEdit(player.id)} type="button" className="btn btn-warning" data-bs-toggle="modal" data-bs-target="#createUser">
                     Edit </button></td>
                </tr>
            ))}
        </tbody>
    </table>
    <br />
    <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createUser">
       Create Player
    </button>
</div>
)
}

export default PlayerList


