import 'bootstrap/dist/js/bootstrap.min.js'
import React, { useEffect } from 'react'
import { useState } from 'react'
const PlayerForm = ({handleCreate, playerEdit, resetForm, performEdit}) => {

const [Email, setEmail] = useState('')
const [Username, setUsername] = useState('')
const [Password, setPassword] = useState('')
const [Exp, setExp] = useState(0)
const [Lvl, setLvl] = useState(0)

    useEffect(() => {
        if(typeof playerEdit === 'object'){
            setEmail(playerEdit.email)
            setUsername(playerEdit.username)
            setPassword(playerEdit.password)
            setExp(playerEdit.exp)
            setLvl(playerEdit.lvl)
        }
    }, [playerEdit])

    const submitHandler = (e) => {
        e.preventDefault()
        let player = {
            email :Email,
            username: Username,
            password: Password,
            exp: Exp,
            lvl: Lvl
        }
        if(typeof playerEdit === 'object' && playerEdit.id)
            performEdit({...player, id: playerEdit.id})
        else
            handleCreate(player)
            handleReset()
    }

    const handleReset = () => {
        resetForm()
        setEmail('')
        setPassword('')
        setUsername('')
        setExp('')
        setLvl('')
    }

return (

<div>
    <div className="modal fade" id="createUser" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
            <form onSubmit={e=> submitHandler(e)}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Create Data Player</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className="mb-3">
                            <label htmlFor="username" className="form-label">Username</label>
                            <input onChange={(e)=> setUsername(e.target.value)} type="text" className="form-control"
                            id="username" value={Username} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">Email address</label>
                            <input type="email" onChange={(e)=> setEmail(e.target.value)} className="form-control"
                            id="email" value={Email} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="password" className="form-label">Password</label>
                            <input type="password" onChange={(e)=> setPassword(e.target.value)} className="form-control"
                            id="password" value={Password} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exp" className="form-label">Experience</label>
                            <input type="number" onChange={(e)=> setExp(e.target.value)} className="form-control"
                            id="exp" value={Exp} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="lvl" className="form-label">Lvl</label>
                            <input type="number" onChange={(e)=> setLvl(e.target.value)} className="form-control"
                            id="lvl" value={Lvl} />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Submit</button> &nbsp;
                        <button type="button" onClick={handleReset} className="btn btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

)

}

export default PlayerForm