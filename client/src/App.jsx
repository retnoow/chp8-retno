import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/style.css'
import PlayerList from './components/PlayerList';
import PlayerForm from './components/PlayerForm';
import { useState } from 'react';

function App() {

  let defaultPlayer = [
    {
        id: 1,  
        username: 'admin',
        email: "admin@app.com",
        password: "admin",
        exp: 10,
        lvl: 1
    },
    {
        id: 2, 
        username: 'admin2',
        email: "admin2@app.com",
        password: "admin2",
        exp: 11,
        lvl: 2
    },
    {
      id: 3, 
      username: 'player1',
      email: "player1@app.com",
      password: "player1",
      exp: 38,
      lvl: 10
    }
    
  ]

const [Players, setPlayers] = useState(defaultPlayer)
const [PlayerEdit, setPlayerEdit] = useState('')
const [Keyword, setKeyword] = useState()

const addPlayer = (newPlayer) => {
  newPlayer.id = Math.max(...Players.map(player => player.id))+ 1
  setPlayers(Players => [...Players, newPlayer])
}

const selectEdit = (playerId) => {
  let player = Players.filter(player => player.id === playerId)
  console.log(player[0])
  setPlayerEdit(player[0]) 
}

const performEdit = (player) => {
  console.log(`edit player with uname ${player.username}`)
  const indexEdit = Players.findIndex(item => item.id === player.id)
  console.log(`player index to be edited is ${indexEdit}`)
  
  let copyPlayer = [...Players]
  copyPlayer[indexEdit] = player
  setPlayers(copyPlayer)
}

const resetForm = () => {
  setPlayerEdit('')
}

const filterData = () =>{
  return Players.filter(player => new RegExp(Keyword, 'g').test(player.username) || new RegExp(Keyword, 'g').test(player.email) || 
  new RegExp(Keyword, 'g').test(player.exp)  || new RegExp(Keyword, 'g').test(player.lvl))  
}

return (
  <div>
    <div className='container mt-4'>
      <div className='row'>
        <div className='col-8'>
          <div className='d-flex align-items-center justify-content-between'>
            <h2 className='mt-5 mb-5'>List Data Player</h2>
            <input type='text' className='search form-control justify-content-end col-4' placeholder='search here..' onChange={(e) => setKeyword(e.target.value)} value={Keyword || ''}/>
          </div>
          <div className='modal-body'>
            <PlayerList selectEdit={selectEdit} players={filterData()}/>
          </div>      
        </div>
        <div className='col-4'>
            <PlayerForm handleCreate={addPlayer} playerEdit={PlayerEdit} resetForm={resetForm} performEdit={performEdit}/>
        </div>
      </div>
    </div>
  </div>
);
}

export default App;